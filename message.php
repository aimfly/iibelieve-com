<?php

if(!empty($_POST["markid"])){
	$res=mark_message_as_read($_POST["markid"]);
	if($res[0]){
		echo "{\"result\":\"success\",\"addtext\":\"".get_unread_message_count($userid)."\"}";
		exit();
	}else{
		echo "{\"result\":\"failed\",\"addtext\":\"".$res[1]."\"}";
		exit();
	}
}

function format_message($row,$side){
	$additional_class="";
	if($row['read']==0){
		$additional_class="no_read_message";
	}
	else{
		$additional_class="read_message";
	}
	if($side=='in'){
		echo "<a title='回复' style='color:#000' class='sendmsg' href='?page=sendmsg&to=".$row["from"]."'>";
		echo "<li class='rec_message ".$additional_class." round-top round-bottom'>";
		echo "<input type='hidden' class='msg_id' value='".$row["id"]."'>";
		echo "<span class='message_icon ".$additional_class."_icon'></span>";
		echo "<div class='message_info'>";
		echo "<span class='fromuser'>".get_user_name_by_id($row['from']).":</span>";
		echo "</div>";
		echo "<div class='message_content'>";
		echo $row["content"];
		echo "</div>";
		echo "<div class='message_control'>";
		//echo "<a class='sendmsg' href='?page=sendmsg&to=".$row["from"]."'>reply</a>";
		echo "</div>";
		echo "</li>";
		echo "</a>";
	}
	elseif($side=='out'){
		echo "<li class='send_message round-top round-bottom'>";
		echo "<div class='message_content'>";
		echo $row["content"];
		echo "</div>";
		echo "<div class='message_info'>";
		echo "<span class='touser'>:"."To ".get_user_name_by_id($row['to'])."</span>";
		echo "</div>";
		echo "</li>";
	}
}

?>
<!DOCTYPE unspecified PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<?php the_header();?>
<link href="./css/jqModal.css" media="all" type="text/css" rel="stylesheet">
<script src="js/jquery.js"></script>
<script src="js/jqModal.js"></script>
<script src="js/request.js"></script>
<script type="text/javascript">register_message_page();</script>
</head>
<body>
<?php the_control_panel()?>
<?php the_sendmsg();?>
<div id="wraper">
<?php require_once 'index_cpanel.php';?>
<div id='primary'>
<div style='margin:20px 0 0 20px;text-align:left;'><span style='font-size:1.5em'>你可以给这些人发消息:</span><br>
<?php 
$sql="select * from users where id<>1 and id<>2";
$res=execute_sql($sql);
while($row=get_next_record($res)){
	echo "<div style='text-align:center;float:left;width:20%'><a style='color:#000' class='sendmsg' href='?page=sendmsg&to=".$row["id"]."'>";
	the_user_thumb($row);
	echo "</a></div>";
}
?>
</div>
<div style='clear:both'></div>
<div class='loop_message'>
<ul>
<?php 
    $res=get_message($userid);
    while($row=get_next_record($res)){
    	if($row['to']==$userid){
    	  format_message($row,'in');
    	}
    	if($row['from']==$userid){
    		format_message($row,'out');
    	}
    }
?>
</ul>
</div>
</div>
<?php the_footer();?>
</div>
</body>
</html>