<?php
require_once 'app_basic.php';

if(isset($_GET["app"])){
	$app=$_GET["app"];
}
else{
	$app="home";
}
$current_app=$app;

if(isset($_GET["page"])){
	$page=$_GET["page"];
}
else{
	if($app=="home"){
	    $page="frontpage";
	}
	else{
		$page="index";
	}
}
$current_page=$page;
$current_page=$page;

if(isset($apps[$app])){
	//load app functions
	if($apps[$app]["location"]!="."){
		if(file_exists($apps[$app]["location"].'/functions.php'))include_once $apps[$app]["location"].'/functions.php';
		if(file_exists($apps[$app]["location"].'/dbfunctions.php'))include_once $apps[$app]["location"].'/dbfunctions.php';
	}
	require $apps[$app]["location"]."/".$page.".php";
	exit();
}


?>