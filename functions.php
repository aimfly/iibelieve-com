<?php
require_once 'app_basic.php';
$username='访客';
$userid=-1;
$session_started=false;

function the_footer(){
  require_once 'footer.php';
}
function the_header(){
	echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8">';
	echo '<link href="./css/style.css" media="all" type="text/css" rel="stylesheet">';
}

function the_user_thumb($user){
	echo "<img src='".get_portrait_url($user["portrait"],"thumb",$user['sex'])."'><br>";
	echo $user["name"];
	echo "<span style='margin-left:10px;'><img style='vertical-align: middle;' src='image/".$user["sex"].".png'></span>";
}

function the_control_panel(){
	require_once 'ctlpannel.php';
}

function the_sendmsg(){
	require_once 'sendmsg_daig.php';
}

function init_user(){
 global $username,$userid,$session_started;
 
 date_default_timezone_set('PRC') or die('set timezone failed...');
 if($session_started==false){
  $session_started=true;
  session_start();
 }
 
 if(isset($_SESSION["user"])){
  $username=$_SESSION["user"]["name"];
  $userid=$_SESSION["user"]["id"];
 }

}


function get_portrait_url($img_id,$img_type="",$sex="male"){
	if($img_type!=""||!empty($img_type)){
		$img_type="_".$img_type;
	}
	
	if($img_id==""){
		if($sex=="female"){
		    $img_id="girl_default_portrait";
		}
		else{
			$img_id="boy_default_portrait";
		}
	}
	return "user_portrait/".$img_id.$img_type.".jpg";
}


function get_system_user_id(){
	return 1;
}

function get_admin_user_id(){
	return 2;
}

?>