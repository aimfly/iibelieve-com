<?php
  require 'admin_func.php';
  if($userid!=get_admin_user_id()){
  	die("permission denied");
  }
?>
<!DOCTYPE>
<html>
<head>
<title>
应用管理...
</title>
<style>
th{
  background:#cdcdcd;
}
td{
  padding:5px!important;
}
.td_appname{
   border:1px solid #cdcdcd;
    width:15%;
    text-align:center;
}
.td_showname{
    width:15%;
    border:1px solid #cdcdcd;
    text-align:center;
}
.td_description{
    width:20%;
    border:1px solid #cdcdcd;
    text-align:center;
}

.td_location{
    width:15%;
    border:1px solid #cdcdcd;
    text-align:center;
}
.td_frontpage{
    width:10%;
    border:1px solid #cdcdcd;
    text-align:center;
}
.td_action{
    width:15%;
    border:1px solid #cdcdcd;
    text-align:center;
}
</style>
<?php the_header()?>
<script src="js/jquery.js"></script>
<script>
$(function(){
	$('.installapp').click(function(){
		var app_location=$(this).attr("name");
		var dataString="act=install&app_location="+app_location;
		$.ajax({
			type : "POST",
			url : "?app=admin&page=installapp",
			data : dataString,
			success : function(data) {
				if(data=='success'){
				    document.location.reload();
				}
				else{
					alert("install failed");
				}
			}
		});
	});

	$('.deleteapp').click(function(){
		var app_location=$(this).attr("name");
		var dataString="act=delete&app_location="+app_location;
		$.ajax({
			type : "POST",
			url : "?app=admin&page=installapp",
			data : dataString,
			success : function(data) {
				if(data=='success'){
				    document.location.reload();
				}
				else{
					alert("delete failed");
				}
			}
		});
	});
});
</script>
</head>
<body>
<?php the_control_panel()?>
<div id='wraper'>
<div id='installed_app' style='text-align:left;width:70%;margin:60px auto;'>
<span>已安装的应用：</span><br>
<table style='width:100%'>
<tr>
<th class='td_appname'>名称</th>
<th class='td_showname'>显示名称</th>
<th class='td_description'>描述</th>
<th class='td_location'>位置</th>
<th class='td_frontpage'>首页</th>
<th class='td_action'>操作</th>
</tr>

<?php 
$sql="select * from application";
$res=execute_sql($sql);
if($res){
	while($row=mysql_fetch_array($res)){?>
<tr>
<td class='td_appname'><?php echo $row['name']?></td>
<td class='td_showname'><?php echo $row['showname']?></td>
<td class='td_description'><?php echo $row['description']?></td>
<td class='td_location'><?php echo $row['location']?></td>
<td class='td_frontpage'><?php echo $row['frontpage']?></td>
<td class='td_action'><input type="button" class="deleteapp" name='<?php echo $row['location'];?>' value="卸载"></th>
</tr>
<?php
	}
}
?>

</table>
</div>

<div id='scaned_app' style='text-align:left;width:70%;margin:60px auto;'>
<span>这些应用可安装：</span><br>
<table style='width:100%'>
<tr>
<th class='td_appname'>名称</th>
<th class='td_showname'>显示名称</th>
<th class='td_description'>描述</th>
<th class='td_location'>位置</th>
<th class='td_frontpage'>首页</th>
<th class='td_action'>操作</th>
</tr>
<?php 
if ($handle = opendir('app')) {
while(false !== ($entry = readdir($handle))){
	destroy_app_var();
    if($entry=='.'||$entry=='..'){
    	continue;
    }
    $app_location='app/'.$entry;
    if(!is_dir($app_location)||is_installed($app_location)){
    	continue;
    }
    $app_profile=$app_location."/application.php";
    if(file_exists($app_profile)){
    require $app_profile;
?>
<tr>
<td class='td_appname'><?php echo $app_name;?></td>
<td class='td_showname'><?php echo $app_slug_name;?></td>
<td class='td_description'><?php echo $app_description;?></td>
<td class='td_location'><?php echo $app_location?></td>
<td class='td_frontpage'><?php echo $app_frontpage?></td>
<td class='td_action'><input type="button" class="installapp" name="<?php echo $app_location;?>" value="安装"></td>
</tr>
<?php
    }
  }
}
?>
</table>
</div>


<?php the_footer()?>
</div>
</body>
</html>