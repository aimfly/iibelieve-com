<?php
  require_once 'functions.php';
  global $userid,$username,$current_app;
  if(!isset($current_app)){
  	$current_app="";
  }
  if(!isset($current_page)){
  	$current_page="";
  }
  function match_current_app($str1,$str2){
  	if($str1==$str2){
  		echo "current_app";
  	}
  }
  
  init_user();
?>
<script src="js/ctrlpanel.js"></script>
<script>register_ctrlpannel();</script>
<div id='ctrlpannel'>
  <div id='mainpannel'>
     <div id='changelog' style='min-height:300px;padding:5px;border:1px solid #cdcdcd;position:absolute;width:200px;text-align:left;right:-200px;top:60px;'>
       <div style='background: #cdcdcd;text-align:center'>更新日志</div>
       <div style='color:#000'><pre><?php echo file_get_contents('changelog');?></pre></div>
    </div>
    <div id='main_tip' class='<?php match_current_app("home",$current_page);?>'><a href='<?php the_app_page_url("home","home")?>'>首页</a></div>
    <div id='user_tip'>欢迎您：<?php echo $username?></div>
    <div id='user_op'>
    <ul>
        <?php
           global $apps;
           foreach ($apps as $name => $app){
           	  if($name=='home'||$name=='admin')continue;?>
           	  <li class="app_list_item <?php match_current_app($app["name"],$current_app);?>">
           	  <a href='<?php the_app_page_url($app['name'],$app['frontpage']);?>'><?php echo $app["showname"];?></a>
           	  </li>
        <?php }?>
    <?php if($userid!=-1){?>
        <?php 
          $unread_msg=get_unread_message_count($userid);?>
        <li class="app_list_item <?php match_current_app("message",$current_page);?>">
           <a href='<?php the_app_page_url("home","message")?>'><img src='./image/email.png'><span class='msg_tip' style='padding-left:20px'>消息(<?php echo $unread_msg;?>)</span></a>
        </li>
    <?php }?>
    </ul>
    </div>
    <?php if($userid==-1){?>
    <div id='text_tip'>
    <marquee scrollAmount=3>欢迎访问金灯台家园！</marquee>
    </div>
    <?php }?>
<div id='user_logon_logout'>
<ul>
    <?php if($userid==get_admin_user_id())printf("<li class='log_menu_item'><a href='%s'>应用管理</a></li>",get_app_page_url("admin","appmanager"));?>
    <?php if($userid!=-1){ ?>
<li class="log_menu_item"><a href='?page=logout'>退出！</a></li>
    <?php }else{?>
<li class="log_menu_item" id='login_button' class='text_button'><a href='#'>登陆</a></li>
<li class="log_menu_item <?php match_current_app("register", $current_page)?>"><a href='<?php the_app_page_url("home","register")?>'>没有账户？</a>
<div id='register_tip' class='round-top round-bottom'>
<div class='up_arrow'></div>
<p>开始使用之前请点击这里进行注册</p>
</div>
</li>
</ul>
<div id='quick_login' class='fixedtext round-top no_display round-bottom shadow_bottom'>
<form method="post"><input type="hidden" name="reqType" value="logon">
<table id='quick_login_table'>
	<tr>
		<th colspan=2 id='logontip'></th>
	</tr>
	<tr>
		<td><label for="email">邮箱：</label></td>
		<td><input type="text" id='email' name="email" /></td>
	</tr>
	<tr>
		<td><label for="password">密码：</label></td>
		<td><input type="password" id='password' name="password" /></td>
	</tr>
	<tr>
		<td colspan=2 style="text-align:center" class='bottom_td'>
		<input type="button" id="logon" value="登陆" />
		</td>
	</tr>
</table>
</form>
</div>

<?php  }  ?>

　　</div>
  </div>
</div>