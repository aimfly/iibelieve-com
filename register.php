<?php
$current_page="register";
?>
<!DOCTYPE>
<html>
    <head>
        <title>注册...</title>
        <?php the_header();?>
        <script src="js/jquery.js"></script>
        <script type="text/javascript" src="js/ajaxfileupload.js"></script>
        <script type="text/javascript" src="js/request.js"></script>
        <script>
        register_logon_page();
        function change_girl_portrait(){
            if($('#portraitid').val()==''){
                $('#portrait_image').attr("src","user_portrait/girl_default_portrait.jpg");
            }
            $('#avatar_tip').html("什么？都不是...");
        }
        function change_boy_portrait(){
        	if($('#portraitid').val()==''){
                $('#portrait_image').attr("src","user_portrait/boy_default_portrait.jpg");
        	}
            $('#avatar_tip').html("什么？都不是...");
        }

        function change_avatar_portrait(){
        	if($('#portraitid').val()==''){
        	    $('#portrait_image').attr("src","user_portrait/avatar_default_portrait.jpg");
        	}
        	$('#avatar_tip').html("Kidding...??");
        }
        </script>
        <style>
        /*IE6/7*/   
#preloader {   
/* Images you want to preload*/   
background-image: url('user_portrait/boy_default_portrait.jpg');
background-image: url('user_portrait/girl_default_portrait.jpg');
background-image: url('user_portrait/avatar_default_portrait.jpg');
width: 0px;   
height: 0px;   
display: inline;   
}
/*preload for other Browser*/   
body:after   
{   
content: url('user_portrait/boy_default_portrait.jpg') url('user_portrait/girl_default_portrait.jpg') url('user_portrait/avatar_default_portrait.jpg');   
display: none;   
}   
        </style>
    </head>
    <body>
    <!--[if (IE 6)|(IE 7)]>  
       <div id="preloader"></div>  
    <![endif]-->  
    <?php include_once 'ctlpannel.php';?>
    <div id='wraper'>
      <div id='registerform'>
      <div class='fixedtext shadow_bottom'>
      <form method="post">
        <input type="hidden" name="reqType" value="register">
      <table>
        <tr><th colspan=2 id='registertip' class='tb_title_bg_color'>注册新用户</th></tr>
        <tr><td class='right_td' style='text-align:center;'><div id='portrait'><img id='portrait_image' src='user_portrait/boy_default_portrait.jpg'><input size=10 id="fileToUpload" type="file" name="fileToUpload"><input type="hidden" name='portraitid' id='portraitid' value=''></div></td>
            <td style='text-align: left;' class='sex_sel'>
                <label><input type="radio" name='sex' value='male' checked='checked' onclick='change_boy_portrait();'>弟兄</label><br><br>
                <label><input type="radio" name='sex' value='female' onclick='change_girl_portrait();'>姐妹</label><br><br>
                <label><input type="radio" name='sex' value='unknown' onclick='change_avatar_portrait();'><span id='avatar_tip'>什么？都不是...</span></label>
            </td>
         </tr>
        <tr><td class='right_td'><label for="regEmail">注册邮箱：</label>
        <input type="text" id='regEmail' name="regEmail" /></td><td class='mail_tip'>在此处输入您的注册邮箱</td></tr>
        <tr><td class='right_td'><label for="passwd">新密码　：</label>
        <input type="password" id='passwd' name="passwd" /></td><td class='pw_tip'>请在此处输入您的注册密码</td></tr>
        <tr><td class='right_td'><label for="re-passwd">确认密码：</label>
        <input type="password" id='re-passwd' name="re-passwd" /></td><td class='repw_tip'><label id='regPass_icon'></label>请再次输入您的注册密码</td></tr>
        <tr><td class='right_td'><label for="username">昵　　称：</label>
        <input type="text" id='username' name="username" /></td><td class='name_tip'>请在此处输入您的昵称</td></tr>
        <tr><td class='right_td'><label for="invitecode">邀请码　：</label>
        <input type="text" id='invitecode' name="invitecode" /></td><td class='invite_tip'>请输入教会的名称</td></tr>
        <tr><td colspan=2  class='normal_td bottom_td'><center><input type="button" id="register" value="注册" /></center></td></tr>
      </table>
      </form>
    </div>
    </div>
        <?php the_footer();?>
    </div>
    

    </body>
</html>