<!DOCTYPE>
<html>
<head>
<title>good day,isn't it</title>
<?php the_header();?>
<script src="js/jquery.js"></script>
<script src="js/request.js"></script>
<script type="text/javascript">
var load_running=0;
function loadPost(){
	var offset=$('#post_bottom').offset();
	var scrT=$(document).scrollTop();
	var win_height=$(window).height();
	if(scrT+win_height>offset.top){
		if(load_running==1){
	        return;
	    }
	    else{
	    	load_running=1;
	    }
		if($('#post_bottom').hasClass("no_display")){
			load_running=0;
			return;
		}
	    $('#post_bottom').html("<img src='image/loading.gif'>内容读取中...");
        var count=$('#count').val();
	    var dataString="queryType=getCount&offset="+count;
	    $.ajax({
			type:"POST",
			url:"?page=query_post",
			data:dataString,
			dataType: 'json',
			success:function(data,status){
				if(data.result=="failed"){
			      $('#post_bottom').html("");
				}
				else{
					if(data.count<10){
						$('#post_bottom').addClass("no_display");
					}
					var dataString="offset="+count;
					$.ajax({
						type:"POST",
						url:"?page=query_post",
						data:dataString,
						success:function(data){
							$('#home_posts').append(data);
							$('.new_post').hide();
							$('.new_post').fadeIn("slow");
							$('.new_post').removeClass("new_post");
							load_running=0;
						}
					});
					$('#count').val(parseInt(data.count)+parseInt(count));
				}
		    },
		    error: function (data, status, e)
            {
		    	load_running=0;
                alert(e);
            }
		});
	}
}


$(function(){
	loadPost();
	$(window).scroll(loadPost);
});
</script>
</head>
<body>
<?php require_once 'ctlpannel.php';?>
<div id='wraper'>
<?php require_once 'index_cpanel.php';?>

<div id='primary'>
<span class='round-top round-bottom' style='padding:5px;border:1px solid #cdcdcd;text-align:left;display:block;margin:20px 0 0 10px;width:80%;'>这个网站的目的是为了提供给金灯台教会的dxjm一个网上交流的平台，同时提供一些教会日常事务的管理，例如图书管理，目前还在设计中，一个简单的功能现在已经可以使用:<a href='loop.php'>二手物品的分享和交换</a>，如果您想参与这个站点的建设，或是有什么好的建议，请<a href='mailto:aimfly@gmail.com'>联系我</a></span>

<ul id='home_posts'></ul>
<input type="hidden" id="count" value=0>
<div id='post_bottom'></div>
</div>
<?php require_once 'footer.php';?>
</div>
</body>
</html>