<?php
require_once 'dbfunc.php';
$apps=array();
$current_app="";
$sql="select * from application";
$res=execute_sql($sql);
if($res){
	while($row=get_next_record($res)){
		$apps[$row['name']]=$row;
	}
}

$apps["home"]=array();
$apps["home"]["location"]=".";
$apps["admin"]=array();
$apps["admin"]["location"]="./admin";

function set_current_app($app){
	global $current_app;
	$current_app=$app;
}
function load_content($app,$page){
	$loc=get_app_location($app);
	require_once $loc.'/'.$page.'.php';
}
function get_app_location($app=""){
	global $current_app,$apps;
	if(empty($app)){
		$app=$current_app;
	}
	if(isset($apps[$app])){
		return $apps[$app]["location"];
	}
	else {
		return "";
	}
}

function the_app_location($app=""){
	echo get_app_location($app);
}

function get_app_page_url($app="",$page="index"){
	global $current_app;
	if(empty($app)){
		$app=$current_app;
	}
	if(!isset($apps[$app])){
		return "?app=".$app."&page=".$page;
	}
	else {
		return "";
	}
}

function the_app_page_url($app="",$page="index"){
	echo get_app_page_url($app,$page);
}

?>