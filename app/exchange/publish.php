<?php

$current_app="exchange";
$current_page="publish";
if($userid==-1){
 header("Location: .");
}

if(isset($_POST["photoid"])){
	unlink(get_img_url($_POST["photoid"]));
	unlink(get_img_url($_POST["photoid"],"thumb"));
	unlink(get_img_url($_POST["photoid"],"show"));
	exit();
}

if(!empty($_POST["state"])){
 //already post 
 if(empty($_POST["title"])){
  echo "title is empty";
  exit();
 }
 else{
  //for test
  $title="'".$_POST["title"]."'";
  if(empty($_POST["photoLoc"])){
   $photoloc="null";
  }
  else{
   $photoloc="'".$_POST["photoLoc"]."'";
  }
  if(empty($_POST["description"])){
   $desciption="null";
  }
  else{
   $desciption="'".$_POST["description"]."'";
  }
  if(empty($_POST["evaluation"])){
   $evaluation="null";
  }
  else{
   $evaluation="'".$_POST["evaluation"]."'";
  }
  
  if(empty($_POST["canGive"])){
  	$cangive=0;
  }
  elseif($_POST["canGive"]=="yes"){
  	$cangive=1;
  }
  else{
  	$cangive=0;
  }

  $sql=mysql_query("insert into exc_goods (createtime,user_id,title,description,photo_loc,evaluation,cangive) values"."(now(),"
            .$userid.",".$title.",".$desciption.",".$photoloc.",".$evaluation.",".$cangive.")");
  if(!$sql){
   echo mysql_error();
   
   exit();
  }
  else{
   $goods_id=mysql_insert_id();
   $post_content='<span style="margin-left:10%;float:left"><img src="'.get_img_url($_POST["photoLoc"],"show").'"></span><br><span style="margin-left:5%;float:left">'.get_user_name_by_id($userid).'在<a href="?app=exchange&page=loop">物品交换</a>中发布了'.'<a href="'.get_app_page_url("exchange","item").'&id='.$goods_id.'">'.$_POST["title"]."</a></span>";
   create_post(array("type"=>19,"userid"=>get_system_user_id(),"content"=>$post_content));
echo <<<EOF
<HTML>
<HEAD>
<TITLE>插入成功，2秒后自动跳转</TITLE>
<meta http-equiv="Refresh" content="2; url=?app=exchange&page=loop&type=mine">
</HEAD>
<body>插入成功，2秒后自动跳转</body>
</HTML>
EOF;
   echo "insert success";
   exit();
  }
  
 }
}
?>
<!DOCTYPE unspecified PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <title>发布...</title>
        <?php the_header();?>
        <link href="<?php the_app_location()?>/css/style.css" media="all" type="text/css" rel="stylesheet">
        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/ajaxfileupload.js"></script>
        <script src="<?php the_app_location()?>/js/exchange.js"></script>
        <script type="text/javascript">register_publish_page();</script>
    </head>
    <body>
    <?php the_control_panel();?>
    <div id='wraper'>
    <?php require_once 'exchange_cpanel.php';?>
    <div id='primary'>
    <div id='publishform'>
    <div class='shadow_bottom'>
    <table>
    <tr><th style="border-bottom:0px;" class="tb_title_bg_color" colspan=2>请填写物品信息：</th></tr>
    <tr>
    <td colspan=2 style="border-top:0px;" class="imageupload">
    <div class="upimage"><span style='float:left'>上传物品图片</span>
    <div class='uploadbutton'><input size=10 id="fileToUpload" type="file" name="fileToUpload"></div></div>
    <img id="loading" src="<?php the_app_location()?>/image/loading.gif" style="display:none;">
    <div id="photoreview"></div>
    <span class='reupload' style="display:none;cursor:pointer;">重新选择图片（为了优化，图片可能经过裁剪）</span>
    </td>
    </tr>
    <form action="<?php the_app_page_url("exchange","publish")?>" method="post">
        <input type="hidden" name="state" value="post">
        <input type="hidden" id="photoLoc" name="photoLoc" value=""> 
        <tr><td colspan=2 class="tb_content_bg_color"><label for="title"><font class="must_fill">*</font>物品标题：</label><input type="text" size=60 id="title" name="title" /></td></tr>
        <tr><td colspan=2 class="tb_content_bg_color"><div style="float:left"><label for="description">物品描述：</label></div><textarea rows=10 cols=79 id="description" name="description"></textarea></td></tr>
        <tr><td  colspan=2 class="tb_content_bg_color"><label for="evaluation">估　　价：</label><input type="text" id="evaluation" name="evaluation" /></td></tr>
        <tr><td  colspan=2 class="tb_content_bg_color"><input type="checkbox" name="canGive" value="yes" checked="checked"><span class='must_fill'>可否赠送</span></td></tr>
        <tr><td colspan=2 class="tb_content_bg_color"><input id='pub_bottom' type="submit" value="发布" /></td>
    </form>
    </table>
    </div>
    </div>
    </div>
    <?php the_footer();?>
    </div>
</body>
</html>