<?php
include_once 'image_process.php';
	$error = "";
	$msg = "";
	$new_file_url="";
	$fileElementName = 'fileToUpload';
	$show_img_url="";
	$img_id="";
	if(!empty($_FILES[$fileElementName]['error']))
	{
		switch($_FILES[$fileElementName]['error'])
		{

			case '1':
				$error = 'The uploaded file exceeds the upload_max_filesize directive in php.ini';
				break;
			case '2':
				$error = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
				break;
			case '3':
				$error = 'The uploaded file was only partially uploaded';
				break;
			case '4':
				$error = 'No file was uploaded.';
				break;

			case '6':
				$error = 'Missing a temporary folder';
				break;
			case '7':
				$error = 'Failed to write file to disk';
				break;
			case '8':
				$error = 'File upload stopped by extension';
				break;
			case '999':
			default:
				$error = 'No error code avaiable';
		}
	}elseif(empty($_FILES['fileToUpload']['tmp_name']) || $_FILES['fileToUpload']['tmp_name'] == 'none')
	{
		$error = 'No file was uploaded..';
	}elseif($_FILES['fileToUpload']['type']!="image/jpeg" && $_FILES['fileToUpload']['type']!="image/pjpeg"){
	    $error = 'Input file is not jpeg file..';
	}else
	{
    
	        $msg .= " File type: " . $_FILES['fileToUpload']['type'] . ", ";
			$msg .= " File Name: " . $_FILES['fileToUpload']['name'] . ", ";
			$msg .= " File Size: " . @filesize($_FILES['fileToUpload']['tmp_name']);
			//for security reason, we force to remove all uploaded file
			$img_id=date('Y-m-d-i-s');
			$new_file_url=get_app_location("exchange")."/upload/" .$img_id.".jpg";
			$thumb_img_url=get_app_location("exchange")."/upload/" .$img_id."_thumb.jpg";
			$show_img_url=get_app_location("exchange")."/upload/" .$img_id."_show.jpg";
			move_uploaded_file($_FILES["fileToUpload"]["tmp_name"],$new_file_url);
            convert_image($new_file_url, $thumb_img_url, 60, 60);
            convert_image($new_file_url, $show_img_url,310,310);
			//@unlink($_FILES['fileToUpload']);		
	}		
	echo "{";
	echo				"error: '" . $error . "',\n";
	echo				"msg: '" . $show_img_url . "',\n";
	echo                "photoid: '" .$img_id . "'\n";
	echo "}";
?>