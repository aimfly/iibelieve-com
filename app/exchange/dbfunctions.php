<?php

function get_request_count($goodsid){
	$sql="select count(*) from exc_transaction where status=1 and goods_id1= ".$goodsid;
	$result=mysqli_query($dbconn,$sql);
	if (!$result) {
		echo mysqli_error($dbconn);
		return 0;
	}else{
		$row=mysqli_fetch_array($result);
		return $row[0];
	}
}

function can_give($goods_id){
	$sql="select cangive from exc_goods where id=".$goods_id;
	$result=mysqli_query($dbconn,$sql);
	if (!$result) {
		echo mysqli_error($dbconn);
		return false;
	}else{
		$row=mysqli_fetch_array($result);
		return $row[0];
	}
}

function get_goods_count($userid,$search_key){
	if($userid!=-1){
		$user_cond="user_id=".$userid;
		$status_cond="";
	}
	else{
		$user_cond="";
		$status_cond="status=1";
	}

	if($search_key!=""){
		$search_cond="title like '%".$search_key."%'";
	}
	else{
		$search_cond="";
	}

	$cond_string=combine_condition($user_cond,$status_cond,$search_cond);

	$sql="select count(*) from exc_goods ".$cond_string;
	$res=execute_sql($sql);
	if($res){
		$row=mysqli_fetch_array($res);
		return $row[0];
	}
	else{
		return $res;
	}
}


function get_goods_list($userid,$search_key,$offset,$number){
	if($userid!=-1){
		$user_cond="user_id=".$userid;
		$status_cond="";
	}
	else{
		$user_cond="";
		$status_cond="status=1";
	}

	if($search_key!=""){
		$search_cond="title like '%".$search_key."%'";
	}
	else{
		$search_cond="";
	}

	$cond_string=combine_condition($user_cond,$status_cond,$search_cond);

	$sql="select * from exc_goods ".$cond_string." order by createtime desc limit ".$offset.",".$number;
	return execute_sql($sql);
}

function get_goods_by_id($goods_id){
	$sql="select * from exc_goods where id=".$goods_id;
	$res=mysqli_query($dbconn,$sql);
	if(!$res){
		echo mysqli_error($dbconn);
		return false;
	}else{
		$row=mysqli_fetch_array($res);
		return $row;
	}
}

function request_send($goods_id){
	$req_send=false;
	$sql="select * from exc_transaction where exc_transaction.goods_id2=".$goods_id." AND exc_transaction.status=1";
	$res=mysqli_query($dbconn,$sql);
	if(mysqli_num_rows($res)!=0){
		$row=mysqli_fetch_array($res);
		return $row;
	}
	return $req_send;
}

function transaction_over($goods_id){
	$trans_ok=false;
	//check if goods id at target goods in done transaction
	$sql="select * from exc_transaction where exc_transaction.goods_id1=".$goods_id." AND exc_transaction.status=2";
	$res=mysqli_query($dbconn,$sql);
	if(mysqli_num_rows($res)!=0){
		$trans_ok=true;
	}
	//check if goods id at request goods in done transaction
	$sql="select * from exc_transaction where exc_transaction.goods_id2=".$goods_id." AND exc_transaction.status=2";
	$res=mysqli_query($dbconn,$sql);
	if(mysqli_num_rows($res)!=0){
		$trans_ok=true;
	}
	return $trans_ok;
}
?>