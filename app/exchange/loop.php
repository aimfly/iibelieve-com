<?php
$cur_p=1;
$item_per_page=8;
$url_prefix=get_app_page_url("exchange","loop")."&";

$current_app="exchange";
$current_page="loop";

if(!empty($_POST["skey"])){
	$search_key=$_POST["skey"];
	$url_prefix.="skey=".$search_key."&";
}
else{
	$search_key="";
}
$search_user=-1;
if(!empty($_GET["type"])){
	if($_GET["type"]=="mine"){
		if($userid==-1){
			echo "user is not logged in";
			exit();
		}
		else{
			$current_page="mine";
		    $search_user=$userid;
		    $url_prefix.="type=mine&";
		}
	}
}

$total_count=get_goods_count($search_user,$search_key);
if(empty($_GET["p"])){
	$cur_p=1;
	$result = get_goods_list($search_user,$search_key,0,$item_per_page);
}
else{
	$cur_p=$_GET["p"];
	$result = get_goods_list($search_user,$search_key,($cur_p-1)*$item_per_page,$item_per_page);
}

if(!$result){
	exit();
}

?>
<!DOCTYPE>
<html>
<head>
<?php the_header();?>
<link href="<?php the_app_location()?>/css/style.css" media="all" type="text/css" rel="stylesheet">
<title><?php 
   if(!empty($_GET["type"]) && $_GET["type"]=="mine"){
   	  echo "我发布的物品";
   }
   else{
   	  echo "物品列表";
   }
?></title>
<script src="js/jquery.js"></script>
</head>
<body>
<?php the_control_panel();?>
<div id="wraper">
<?php require_once 'exchange_cpanel.php';?>
<div id="primary">
<div id="search_form">
<form method="POST" action="<?php the_app_page_url("exchange","loop")?>">
<?php 
if($search_user!=-1){
    echo "<input type='hidden'' name='type'' value='mine'>";
}
if(!empty($_GET["skey"])){
	echo "<input type='text' size='60' name='skey' value='".$_GET["skey"]."'>";
}else{
	echo "<input type='text' size='60' name='skey'>";
}

?>

<input type="submit" value="搜物">
</form>
</div>
<div id="loop">
   <ul class="loop-tb">
   <li class="title">
   <span style='float:left'> 
<?php 
if(!empty($_GET["type"])){
	if($_GET["type"]=="mine"){
		if($search_key!=""){
			echo "在您发布的物品中搜到<span class='striking_text'> ".$total_count."</span> 件与 ".$search_key." 匹配的物品";
		}else{
			echo "您一共发布了 ".$total_count." 件物品";
		}
		
	}
}
else{
    if($search_key!=""){
		echo "搜到<span class='striking_text'> ".$total_count."</span> 件与 ".$search_key." 匹配的物品";
	}
	else{
	    echo "物品列表：";
	}
}
?>
</span>
   <span class="top_right_nevi">
   
   <?php
       generate_navigation($url_prefix,$cur_p,$total_count,$item_per_page);
   ?>
   </span>
   <div style="clear:both"></div>
   </li>
   <?php 
       if(mysqli_num_rows($result)==0){
         echo "没有物品记录";
       }
       else{
         while($row=mysqli_fetch_array($result)) {
           if(empty($_GET["type"]) ){
               show_goods_item_default($row);
           }elseif ($_GET["type"]=="mine"){
           	   show_mine_goods_item_default($row);
           }
         }
       }
   ?>
   </ul>
</div>
</div>
<div style="clear:both;"></div>
<?php the_footer();?>
</div>
</body>
</html>