<?php

function get_img_url($img_id,$img_type=""){
	if($img_type!=""||!empty($img_type)){
		$img_type="_".$img_type;
	}
	if(empty($img_id)){
		return get_app_location("exchange")."/upload/noimage.gif";
	}else{
	    return get_app_location("exchange")."/upload/".$img_id.$img_type.".jpg";
	}
}



function generate_navigation($pre_URL,$cur_page,$total_count,$count_perpage){
	if($cur_page!=1){
		echo "<div class='left_nevi'><a href='".$pre_URL."p=".($cur_page-1)."'>上一页</a></div>";
	}
	if($cur_page*$count_perpage<$total_count){
		echo "<div class='right_nevi'><a href='".$pre_URL."p=".($cur_page+1)."'>下一页</a></div>";
	}
}

function show_mine_goods_item_default($row){
	echo "<li class='item_list'>";
	echo "<div class='photo'>";
    $photo_loc=get_img_url($row["photo_loc"],"thumb");
	echo "<img class='litter_goods_pic' src='".$photo_loc."'/>";
	echo "</div>";
	echo "<div class='status'>";
	$transaction_done=transaction_over($row["id"]);
	if($transaction_done){
		echo "<p class='green_text'>交易已成功</p>";
	}
	else{
		$request_send=request_send($row["id"]);
		if($request_send){
			echo "<p class='striking_text'>";
			echo "已发出交换请求";
			echo "</p>";
			echo "<p>";
			$goods=get_goods_by_id($request_send["goods_id1"]);
			echo "<a href='".get_app_page_url("exchange","item")."&id=".$goods["id"]."'>";
			echo $goods["title"];
			echo "</a>";
			echo "</p>";
		}
		else{
			echo "<p>";
			echo "未发出交换请求";
			echo "</p>";
		}
		
	    echo "<p>";
	    echo "请求数量：".get_request_count($row["id"]);
	    echo "</p>";
	}
	echo "</div>";
	echo "<a href='".get_app_page_url("exchange","item")."&id=".$row["id"]."'>";
	echo  $row["title"];
	echo "</a>";
	echo "<p style='position:absolute;left:90px;top:60px'>";
	echo $row["createtime"];
	echo "</p>";
	echo "</li>";
}

function show_goods_item_default($row){
	echo "<li class='item_list'>";
	echo "<div class='photo'>";
	$photo_loc=get_img_url($row["photo_loc"],"thumb");
	echo "<img class='litter_goods_pic' src='".$photo_loc."'/>";
	echo "</div>";
	echo "<div class='status'>";
	echo "<p>";
	$username=get_user_name_by_id($row["user_id"]);
	echo "提供者：".$username;
	echo "</p>";
	echo "<p>";
	echo "估价：".$row["evaluation"];
	echo "</p>";
	echo "<p>";
	echo "请求数量：".get_request_count($row["id"]);
	echo "</p>";
	echo "</div>";
	echo "<div class='goods_feature'>";
	if($row["cangive"]==1){
		printf("<img title='该物品可赠送' src='%s/image/given.png'>",get_app_location());
	}
	echo "</div>";
	echo "<a href='".get_app_page_url("exchange","item")."&id=".$row["id"]."'>";
	echo  $row["title"];
	echo "</a>";
	echo "<p style='position:absolute;left:90px;top:60px'>";
	echo $row["createtime"];
	
	echo "</p>";
	echo "</li>";
}
?>