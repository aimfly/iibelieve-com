function register_publish_page(){
	$(function(){
	    $(".uploadbutton").mousemove(function(e) {
	        var offL, offR, inpStart
	        offL = $(this).offset().left;
	        offT = $(this).offset().top;
	        aaa= $(this).find("input").width();
	        $(this).find("input").css({
	            left:e.pageX-offL-100,
	            top:e.pageY-offT-10
	        })
	    });
	    $('#pub_bottom').click(function(){
	    	$('#pub_bottom').val("发送中...");
	    });
	    $(window).unload(function(){
	    	var key=$('#pub_bottom').val();
	    	var img_id=$('#photoLoc').val();
	    	if(key!="发送中..."){
	    		if(img_id!=""){
	    	      var dataString="photoid="+img_id;
	    		  $.ajax({
	    			 type:"POST",
	    			 url:"publish.php",
	    			 data:dataString,
	    			 sucess:function(data){
	    			 	;
	    		 	 }
	    		  });
	    		}
	    	}
	    });
	    $('.reupload').click(function(){
	    	 $('#photoreview').html("");
	    	 var img_id=$('#photoLoc').val();
	    	 if(img_id!=""){
	    		 var dataString="photoid="+img_id;
	    		 $.ajax({
	    			type:"POST",
	    			url:"publish.php",
	    			data:dataString,
	    			sucess:function(data){
	    				;
	    			}
	    		 });
	    	 }
             $('#photoLoc').val("");
	    	$('.upimage').show();
            $('.reupload').hide();
	    });
        $("#fileToUpload").change(function(){
        	uploadGoodsPhoto();
        });
    });
}


function uploadGoodsPhoto(){
	$('#photoreview').html("");
	$("#loading").show();
	$.ajaxFileUpload(
            { url:'?app=exchange&page=uploadphoto',
               secureuri:false,
               fileElementId:'fileToUpload',
               dataType: 'json',
               success: function (data, status)
               {
                   $("#loading").hide();
                   if(typeof(data.error) != 'undefined')
                   {
                       if(data.error != '')
                       {
                           alert(data.error);
                       }else
                       {
                           $('#photoreview').html("<img src='"+data.msg+"'>");
                           $('#photoLoc').val(data.photoid);
                           $('.upimage').hide();
                           $('.reupload').show();
                       }
                   }
                   $("#fileToUpload").change(function(){
                	   uploadGoodsPhoto();
                   });
                   return false;
               },
               error: function (data, status, e)
               {
                   $("#loading").hide();
                   $("#fileToUpload").change(function(){
                	   uploadGoodsPhoto();
                   });
               }
            }
           );
}

function register_getlist_page(){
	$(function() {
		$(".select_button").click(function(){
			document.getElementById("select_button").disabled = true;
			var target_id=$('#target_id').val();
			var request_id=$(":checked").val();
			var select_num=$(":checked").length;
			if(select_num==0){
				alert("未选择任何物品");
				document.getElementById("select_button").disabled = false;
				return;
			}
			var dataString="target_id="+target_id+"&goods_id="+request_id+"&requestType=create";
			$.ajax({
				type : "POST",
				url : "?app=exchange&page=request",
				data : dataString,
				success : function(data) {
				    if(data!=""){
				    	$("#wraper").html(data);
				    }
				    else{
					    document.location.reload();
			        }
				}
			});
		});
		
	});
}

function register_item_page(){
	$(function() {
		$(".request_button").click(function() {
			$('#list_contant').html("正在读取可用物品");
			$('#list').slideDown("slow");
			var dataString = "";
			$.ajax({
				type : "POST",
				url : "?app=exchange&page=getlist",
				data : dataString,
				success : function(data) {
					$('#list_contant').html(data);
				}
			});
			return false;
		});
		$(".request_give_button").click(function(){
			var target_id=$('#target_id').val();
			var dataString="target_id="+target_id+"&goods_id=-&requestType=create";
			$.ajax({
				type : "POST",
				url : "?app=exchange&page=request",
				data : dataString,
				success : function(data) {
					document.location.reload();
				}
			});
		});
		$(".close_button").click(function() {
			$('#list').slideUp("slow");
		});
		$(".delete_button").click(function() {
			var trans_id = $(this).attr("name");
			var dataString = "tansactionid=" + trans_id+"&requestType=delete";
			$.ajax({
				type : "POST",
				url : "?app=exchange&page=request",
				data : dataString,
				success : function(data) {
					document.location.reload();
				}
			});
		});
		$(".accept_button").click(function() {
			var trans_id = $(this).attr("name");
			var dataString = "tansactionid=" + trans_id+"&requestType=accept";
			$.ajax({
				type : "POST",
				url : "?app=exchange&page=request",
				data : dataString,
				success : function(data) {
					document.location.reload();
				}
			});
		});
	});
}