<?php
include_once 'functions.php';
init_user();
if(empty($_GET["id"])){
	header("Location: index.php");
	exit();
}
$query_id=$_GET["id"];
$current_app="exchange";
$current_page="item";
$sql="select * from exc_goods where exc_goods.id=".$query_id;
$result = mysql_query($sql);

if(!$result){
	echo mysql_error();
	exit();
}

$item=mysql_fetch_array($result);
if(empty($item)){
	echo("wrong id");
	exit();
}


$photo_loc=get_img_url($item["photo_loc"],"show");

$item_user=get_user_name_by_id($item["user_id"]);
$trans_ok=transaction_over($_GET["id"]);

?>
<!DOCTYPE unspecified PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <title><?php echo($item["title"]); ?></title>
	<?php the_header();?>
	<link href="<?php the_app_location()?>/css/style.css" media="all" type="text/css" rel="stylesheet">
	<script src="js/jquery.js"></script>
	<script src="js/request.js"></script>
	<script src="<?php the_app_location()?>/js/exchange.js"></script>
    <script>register_item_page();</script>
</head>

<body>
<?php the_control_panel();?>
<?php the_sendmsg();?>
  <div id="wraper">
    <?php require_once 'exchange_cpanel.php';?>
    <div id='primary'>
  	<div id="item_detail">
  		<div><span class='item_main_title'><?php echo($item["title"]); ?></span><span class='item_main_addi round-bottom round-top '><a class='sendmsg' title='发送消息给<?php echo $item_user;?>' href='?page=sendmsg&to=<?php echo $item["user_id"];?>'>由 <?php echo($item_user); ?> 提供</a></span></div>
  		<div style='height:20px;clear:both;'></div>
  		<div id="left-column">
  			<img class="goods_pic" src="<?php echo $photo_loc;?>"/>
  		</div>
  		<div id="right-column">
  			<div id="description">
  			    <div class="desc_desc">
  				描述：<br><?php echo($item["description"]); ?>
  				</div>
  				<div class="desc_info">
  				估价：<?php echo($item["evaluation"]); ?>
  				</div>
  			</div>
  			<div id="action">
  			      <input type="hidden" id="target_id" value=<?php echo $_GET["id"] ?>>
  <?php 	  
              if(!$trans_ok){
                 if($userid==-1 || $userid==$item["user_id"]){
                   echo "<input type='button' class='request_button' value='竞换' disabled='disabled'>";
                 }else{
  				   echo "<input type='button' class='request_button' value='竞换'>";
  				   if(can_give($query_id)){
  				     echo "　　<input type='button' class='request_give_button' value='申请赠与'>";
  				   }
                 }
              }else{
                 echo "<input type='button' class='request_button' disabled='disabled' value='交易结束'>";
              }
  ?>
               <div id="list" class='putleft' style="display: none">
                  <div  id="list_control" class="round-top tb_title_bg_color" >可供选择的物品：<a style='float:right;cursor:pointer;' class='close_button'>X</a></div>
                  <div id="list_contant" class="tb_content_bg_color round-bottom"></div>
               </div>
  			</div>
  		</div>
  	</div>
  	

  	<!-- print item which this item reply -->
    <?php
    if($userid==$item["user_id"]) {
      load_content("exchange","reply_item_widget");
    }?>
  	
  	<!-- print items which reply this item  -->
  	<div id="requestlist">
  	<div class="round-top tb_title_bg_color" id="requestlist_title">请求交换的物品：</div>
  	<div id="requestlist_contant" class='round-bottom'>
  	<ul class='request_list_view'>
  	<li class='request_list_item title_ft'>
  	<div class='item_title'>物品</div>
  	<div class='item_request_time_800'>请求时间</div>
  	<div class='item_owner_800'>用户</div>
  	<div class='item_status'>状态</div>
  	</li>
<?php
  $sql="select * from exc_transaction where goods_id1=".$_GET["id"]." order by createtime desc";
  $res=mysql_query($sql);
  if(!$res){
    echo mysql_error();
    exit;
  }
  if(mysql_num_rows($res)==0){
  	echo "<li>暂无交换申请</li>";
  }
  while($trans=mysql_fetch_array($res)) {
  	if($trans["type"]==1){
  		$goods=get_goods_by_id($trans["goods_id2"]);
  		$reply_userid=$goods["user_id"];
  	}
  	elseif($trans["type"]==2){
  		$reply_userid=$trans["goods_id2"];
  	}
?>
  	<li class='request_list_item'>
  	<div class='item_title'>
    <?php
        if($trans["type"]==1){
            echo  "<a href='".get_app_page_url("exchange","item")."&id=".$goods["id"]."'>".$goods["title"]."</a>";
        }
        elseif($trans["type"]==2){
        	echo  "赠与请求";
        }
    ?>
    </div>
    <div class='item_request_time_800'><?php echo $trans["createtime"];?></div>
    <div class='item_owner_800'>
    <a class='sendmsg' href='?page=sendmsg&to=<?php echo $reply_userid;?>'>
    <?php 
          echo get_user_name_by_id($reply_userid);
    ?>
    </a>
    </div>
    <div class='item_status'>
    <?php 
    if($trans["status"]==2){
      echo "交易已被接受";
    }elseif($trans["status"]==3){
      echo "请求已被取消";
    }elseif($reply_userid==$userid && !$trans_ok){
      echo "<input type='button' name='".$trans["id"]."' class='delete_button' value='删除'>";
    }elseif($item["user_id"]==$userid && !$trans_ok){
        if($trans["type"]==1){
            echo "<input type='button' name='".$trans["id"]."' class='accept_button' value='接受'>";
        }
        elseif($trans["type"]==2){
        	echo "<input type='button' name='".$trans["id"]."' class='accept_button' value='赠与'>";
        }
    }
    ?>
    </div>
    </li>
<?php }//end while?>
    </ul>
  	</div>
    </div>
    </div>
    <?php the_footer();?>
    </div>
</body>
</html>