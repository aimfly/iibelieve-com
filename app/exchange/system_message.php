<?php
require_once 'system_message_api.php';

add_message_func("create", "generate_message_create");
add_message_func("accept", "generate_message_accept");

function generate_message_accept($args){
	$type=$args['type'];
	$target_id=$args['target'];
	$goods_id=$args["goods"];
	$target_goods=get_goods_by_id($target_id);
	$from=$target_goods['user_id'];
	if($type==1){
		$request_goods=get_goods_by_id($goods_id);
		$to=$request_goods['user_id'];
		$content=sprintf('%s接受了使用<a href="%s">%s</a>和你交换<a href="%s">%s</a>',get_user_name_by_id($from),
				get_app_page_url("exchange","item")."&id=".$target_id,
				$target_goods['title'],
				get_app_page_url("exchange","item")."&id=".$goods_id,
				$request_goods['title']);
	}elseif ($type==2){
		$to=$goods_id;
		$content=sprintf('%s同意赠送给你<a href="%s">%s</a>',get_user_name_by_id($from),
				get_app_page_url("exchange","item")."&id=".$target_id,
				$target_goods['title']);
	}
	send_message(get_system_user_id(), $to, $content);
}

function generate_message_create($args){
	$type=$args['type'];
	$target_id=$args['target'];
	$goods_id=$args["goods"];
	$target_goods=get_goods_by_id($target_id);
	$to=$target_goods['user_id'];
	if($type==1){
		$request_goods=get_goods_by_id($goods_id);
		$from=$request_goods['user_id'];
		$content=sprintf('%s想用<a href="%s">%s</a>和你交换<a href="%s">%s</a>',get_user_name_by_id($from),
				get_app_page_url("exchange","item")."&id=".$goods_id,
				$request_goods['title'],
				get_app_page_url("exchange","item")."&id=".$target_id,
				$target_goods['title']);
	}elseif ($type==2){
		$from=$goods_id;
		$content=sprintf('%s请求你赠送<a href="%s">%s</a>',get_user_name_by_id($from),
				get_app_page_url("exchange","item")."&id=".$target_id,
				$target_goods['title']);
	}
	send_message(get_system_user_id(), $to, $content);
}

?>