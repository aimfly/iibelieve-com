<?php

$url_prefix=get_app_page_url("library","loop")."&";

$current_app="library";
$current_page="loop";

?>
<!DOCTYPE>
<html>
<head>
<?php the_header();?>
<link href="<?php the_app_location()?>/css/style.css" media="all" type="text/css" rel="stylesheet">
<title>图书馆</title>
<script src="js/jquery.js"></script>
<script type="text/javascript">

function reserve_book(obj,id){
	parent=obj.parentNode;
	parent.removeChild(obj);
	parent.innerHTML="<img style='width:16px;height:16px;' src='image/loading_1.gif'/>";
	$.ajax({
		type:"post",
		url: "?app=library&page=book_oper",
		data:"method=reserve_book&id="+id,
		dataType:'JSON',
		success:function(data){
			if(data.result=="success"){
				 parent.style.color="green";
			     text="预约成功";
			}
			else{
				 parent.style.color="red";
				 text="预约失败";
			}
			setTimeout("parent.innerHTML = text;",500);
	    },
	    error: function (data, status, e)
	    {
	    	alert(e);
	    }
	});
}

function findBook(start)
{
	var key="";
	if($('#searchName').length>0){
		key=$('#searchName').val();
	}
	
	$("#book_list").html("<img src='image/loading.gif'>搜索中...");
	$.ajax({
		type:"post",
		url: "?app=library&page=book_oper",
		data:"method=find_books&q="+key,
		dataType:'JSON',
		success:function(data){
			var i;
			$("#book_list").html("");
			for(i=0;i<data.count;i++){
				var image= "<div class='book_item_block_image'><img  src='"+ data.books[i].image + "'></div>";
				var desc= "<div class='book_item_block_desc'><span class='book_item_block_title'>"
		                   + data.books[i].title
		                   + "</span><span class='book_item_block_author'>"
		                   + data.books[i].author
		                   + "</span></div>";
		        var action="<div><a class='book_item_block_action cursor_hand' onclick='reserve_book(this,"+data.books[i].id+")'>"
		                   + "借阅"
		                   + "</a></div>";
		        if(data.unauthorized){
			        action="";
		        }
				var item= "<li class='book_item_block'>" + image + desc + action + "</li>";
				$("#book_list").append(item);
			}
	    },
	    error: function (data, status, e)
	    {
	    	alert(e);
	    }
	});
	
}

var $$ = function(func){
    var oldOnload =window.onload;
    if(typeof window.onload != 'function'){
        window.onload = func;
    }else{
        window.onload = function(){
            oldOnload();
            func();
        }
    }
}

$$(function(){
	findBook(0);
})




</script>

<style type="text/css">

</style>

</head>
<body>
<?php the_control_panel();?>
<div id="wraper">
<?php require_once 'library_cpanel.php';?>
<div id="primary">
<div id="search_form">
<?php if($userid==-1){?>
<p>登陆后，您可以查询与借阅你所喜爱的图书</p>
<?php }else{?>
<input type='text' size='60' id="searchName" name='skey'>
<button  onclick="findBook(0)">搜索</button>
<?php }?>
</div>
<div id="loop">
   <ul id='book_list' class="loop-tb"/>
</div>
</div>
<div style="clear:both;"></div>
<?php the_footer();?>
</div>
</body>
</html>