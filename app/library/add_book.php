<style type="text/css">

</style>
<script type="text/javascript">
var new_ids=new Array();

function createBookGroup(data,start,end){
	var BookGroup = $("<ul class=\"book_group\" style=\"float:left;\"></ul>");
	var list="";
	for(var i=start;i<end;i++){
		var book=data.books[i]
		var image="<div class='book_item_block_image'><img src='"+book.image+"' /></div>";
		var title="<div class='book_item_block_desc'><span class='book_item_block_title'>书名:"+book.title+" 作者："+book.author+"</span></div>";
		var button="<span class='book_item_block_action'><a class='book_item_block_action_button cursor_hand' onclick=\'addBook(this,\""+book.title+"\",\""+book.author+"\",\""+book.image+"\")\'>添加</a></span>";
		var div="<div style='height:1px; margin-top:-1px;clear:both;overflow:hidden;' />";
		list+="<li class='book_item_block' style='position:relative'>" + image + title + button + div +"</li>";
    }
	BookGroup.html(list);
	return BookGroup;
}


function findBook(start,count){
    var bookName=$('#searchName').val();
    $('#book_list').html("<div style=\"float:left\"><img  src='image/loading.gif'>搜索中...</div>");
    $('#search_navi').html("");
    $('#search_result').html("");
    var url="http://api.douban.com/v2/book/search?q="+bookName+"&start="+start+"&count="+(count*2);
    $.ajax({
		type:"GET",
		url: url,
		dataType:'JSONP',
		success:function(data){
			$('#book_list').html("");
            $('#search_result').html(data.total+" books found");
            var current_start=start;
			$('#book_list').append(createBookGroup(data,0,count));
			$('#book_list').append(createBookGroup(data,count,count*2));

			var navi_left="",navi_right="";
            navi_left=$("<a class='cursor_hand'><img src='image/arrow_left.png'></a>");
            navi_right=$("<a class='cursor_hand'><img src='image/arrow_right.png'></a>");
            
            $('#search_navi').append(navi_left).append(navi_right);


            navi_left.click(function(){
                if(current_start == 0)return;
				$('#book_list').animate({
					left:'+=760px',
					},
					"slow",
					function(){
						current_start = current_start-count;
						if(current_start==0){
							$(".book_group:last").remove();
							$('#book_list').css("left","0px");
						}
						else{
						    var url="http://api.douban.com/v2/book/search?q="+bookName+"&start="+(current_start+count)+"&count="+count;
						    $.ajax({
							    type:"GET",
							    url: url,
							    dataType:'JSONP',
							    success:function(data){
								    $('.book_group:first').before(createBookGroup(data,0,count));
								    $(".book_group:last").remove();
									$('#book_list').css("left","-760px");
							    }
						    });
						}
				     }
				 );
            });
            
            navi_right.click(function(){
                if(current_start+count>=data.total)return;
				$('#book_list').animate({
					left:'-=760px',
					},
					"slow",
					function(){
						if(current_start!=0){
						  $(".book_group:first").remove();
						  $('#book_list').css("left","-760px");
						}
						
						current_start = current_start+count;

						if(current_start+count<data.total){
						  var url="http://api.douban.com/v2/book/search?q="+bookName+"&start="+(current_start+count)+"&count="+count;
						  $.ajax({
							  type:"GET",
							  url: url,
							  dataType:'JSONP',
							  success:function(data){
								  $('#book_list').append(createBookGroup(data,0,count));
                              }
						  });
						}
				});
			});

            
			
			return;
	    },
	    error: function (data, status, e)
        {
	    	alert(e);
        }
	});
}

function addBook(obj,title,author,image){
	parent=obj.parentNode;
	parent.removeChild(obj);
	parent.innerHTML="<img class='small_image' src='image/loading_1.gif'>";
	 $.ajax({
			type:"post",
			url: "?app=library&page=book_oper",
			data:"method=add&title="+title+"&author="+author+"&image="+image,
			dataType:'JSON',
			success:function(data){
				if(data.result=="success"){
					parent.style.color="green";
					text = "添加完成"
					new_ids.push(data.id);
				}
				else{
					parent.style.color="red";
					text = "添加失败"
				}

				setTimeout("parent.innerHTML = text;parent.style.opacity=0;$(parent).animate({margin:'+=10px',opacity:'1',});",500);
				
				return;
		    },
		    error: function (data, status, e)
         {
		    	alert(e);
         }
	});
}

</script>
<div id="search_form">

<input type='text' size='60' id="searchName" name='skey'>
<button  onclick="findBook(0,6)">搜索</button>

</div>
<div id="add_book_loop">
   <div class="add_book_title">
   <span style='float:left' id='search_result'> 
   </span>
   <span class="top_right_nevi" id='search_navi'>
   </span>
   </div>
   <div id='book_list'>
   
   </div>
</div>