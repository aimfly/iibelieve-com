<?php
if($_POST["method"] == "add")
{
	if($userid==-1) {
		header("Location: .");
		exit();
	}

	$book["userid"] = $userid;
	$book["title"] = "'".$_POST["title"]."'";
	$book["image"]="'".$_POST["image"]."'";
	$book["author"]="'".$_POST["author"]."'";
	$result = insert_book_to_db($book);
	
	
	if(!$result[0]){
		$json["result"]="failed";
		$json["error"]=$result[1];
	}
	else{
		$json["result"]="success";
		$json["id"]=get_last_id();
	}	
	die(json_encode($json));
}

if($_POST["method"] == "query_own_books")
{
	if($userid==-1) {
		header("Location: .");
		exit();
	}

	$my_books = get_books_by_userid($userid);
	$json["count"]=get_records_count($my_books);
	$json["books"]=array();
	$i=0;
	while($book=get_next_record($my_books)){
		$json["books"][$i]["id"]=$book["id"];
	    $json["books"][$i]["title"]=$book["title"];
	    $json["books"][$i]["author"]=$book["author"];
	    $json["books"][$i]["borrowBy"]=$book["borrowBy"];
	    if($book["borrowBy"]!=-1){
	    	$json["books"][$i]["borrower"] = get_user_name_by_id($book["borrowBy"]);
	    }
	    $i++;
	}

	die(json_encode($json));
	
}

if($_POST["method"] == "delete_book")
{
	if($userid==-1) {
		header("Location: .");
		exit();
	}

	$result = delete_book_from_db($_POST["id"]);
	
	if(!$result[0]){
		$json["result"]="failed";
		$json["error"]=$result[1];
	}
	else{
		$json["result"]="success";
	}
	
	die(json_encode($json));

}

if($_POST["method"] == "find_books")
{
	if($userid==-1) {
		$json["unauthorized"]=1;
	}
    else{
    	$json["unauthorized"]=0;
    }
	$key="";
	if(isset($_POST["q"])) $key=$_POST["q"];
	
	$result = get_books($key);

	if($result){
		$json["count"]=get_records_count($result);
	    $json["books"]=array();
	    $i=0;
	    while($book=get_next_record($result)){
	    	if(($book["user_id"]==$userid || $book["borrowBy"]!= -1)&&$userid!=-1)continue;
		    $json["books"][$i]["image"]=$book["image"];
	        $json["books"][$i]["title"]=$book["title"];
	        $json["books"][$i]["author"]=$book["author"];
	        $json["books"][$i]["id"]=$book["id"];
	        $i++;
	    }
	}
	else{
		$json["count"]=0;
	}

	die(json_encode($json));

}


if($_POST["method"] == "reserve_book")
{
	if($userid==-1||!isset($_POST["id"])||empty($_POST["id"])) {
		$json["result"]="failed";
		die(json_encode($json));
	}


	$result = reserve_book($userid,$_POST["id"]);

	if($result){
		$json["result"]="success";
	}
	else{
		$json["result"]="failed";
	}

	die(json_encode($json));

}

if($_POST["method"] == "query_borrowed_books")
{
	if($userid==-1) {
		$json["count"]=0;
		die(json_encode($json));
	}


	$result = get_borrowed_books($userid);

	if($result){
		$json["count"]=get_records_count($result);
		$json["books"]=array();
		$i=0;
		while($book=get_next_record($result)){
			$json["books"][$i]["image"]=$book["image"];
			$json["books"][$i]["title"]=$book["title"];
			$json["books"][$i]["author"]=$book["author"];
			$json["books"][$i]["id"]=$book["id"];
			$i++;
		}
	}
	else{
		$json["count"]=0;
	}

	die(json_encode($json));

}

if($_POST["method"] == "return_book")
{
	if($userid==-1) {
		$json["result"]="failed";
		$json["error"]="unauthorized user";
		die(json_encode($json));
	}
    

	if(return_book_by_id($_POST["id"],$userid)!=0){
		$json["result"]="success";
	}
	else{
		$json["result"]="failed";
		$json["error"]="no book or wrong user";
	}

	die(json_encode($json));

}


?>