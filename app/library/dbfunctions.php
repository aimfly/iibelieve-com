<?php
function insert_book_to_db($book)
{
	
	$sql="insert into books (user_id,title,author,image) values"."(".$book["userid"].",".$book["title"].",".$book["author"].",".$book["image"].")";
	return execute_sql_silent($sql);
}


function delete_book_from_db($id)
{
	$sql="delete from books where id=".$id;
	return execute_sql_silent($sql);
}

function get_books($key){
	if(!empty($key)) $condition = " where title like '%".$key."%' OR author like '%".$key."%'";
	else $condition="";
	$sql="select * from books".$condition;
	return execute_sql($sql);
}

function get_book_by_id($id){
	$sql="select * from books where id=".$id;
	$result=execute_sql($sql);
	if($result)
		return get_next_record($result);
}

function return_book_by_id($book_id,$user_id){
	$sql="update books set borrowBy=-1 where id=".$book_id." and user_id=".$user_id;
	$result=execute_sql($sql);
	if($result)
		return 1;
	else 
		return 0;
}


function get_borrowed_books($userid){
	$sql="select * from books where borrowBy=".$userid;
	return execute_sql($sql);
}

function reserve_book($userid,$bookid){
	$sql="select * from books where id=".$bookid;
	$result=execute_sql($sql);
	if($result){
	    $book=get_next_record($result);
	    if($book["borrowBy"]!=-1||$book["user_id"]==$userid)return false;
	    $sql="update books set borrowBy=".$userid." where id=".$bookid;
	    $result=execute_sql($sql);
	    if($result)return true;
	    else return false;
	}
	else{
		return false;
	}
}

function get_books_by_userid($userid){
	$sql="select * from books where user_id=".$userid;
	return execute_sql($sql);
}


?>