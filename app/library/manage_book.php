<?php

$url_prefix=get_app_page_url("library","manage_book")."&";

$current_app="library";
$current_page="manage_book";

?>
<!DOCTYPE>
<html>
<head>
<?php the_header();?>
<link href="<?php the_app_location()?>/css/style.css" media="all" type="text/css" rel="stylesheet">
<link href="css/jqModal.css" media="all" type="text/css" rel="stylesheet">
<title>图书馆</title>
<script src="js/jquery.js"></script>
<script src="js/jqModal.js"></script>
<script type="text/javascript">


function delete_book(id,position){
	$.ajax({
		type:"post",
		url: "?app=library&page=book_oper",
		data:"method=delete_book&id="+id,
		dataType:'JSON',
		success:function(data){
			if(data.result=="success"){
			   var list=document.getElementById("my_books");
			   list.removeChild(document.getElementById("my_book_"+position));
			}
			else{
				alert(data.error);
			}
			
	    },
	    error: function (data, status, e)
	    {
	    	alert(e);
	    }
	});
}


function return_book(id,position){
	$.ajax({
		type:"post",
		url: "?app=library&page=book_oper",
		data:"method=return_book&id="+id,
		dataType:'JSON',
		success:function(data){
			if(data.result=="success"){
			   var book=document.getElementById("my_book_"+position);
			   book.childNodes[3].innerHTML="未借"
			   book.childNodes[2].innerHTML="<a href='javascript:delete_book("+id+","+position+")'>删除</a>";
			}
			else{
				alert(data.error);
			}
			
	    },
	    error: function (data, status, e)
	    {
	    	alert(e);
	    }
	});
}

function refresh_my_books(){
$.ajax({
	type:"post",
	url: "?app=library&page=book_oper",
	data:"method=query_own_books",
	dataType:'JSON',
	success:function(data){
		var i;
		$("#my_books").html("");
		for(i=0;i<data.count;i++){
			var status_str="未借";
			var oper_str="<a href='javascript:delete_book("+data.books[i].id+","+i+")'>删除</a>";
			if(data.books[i].borrowBy!=-1){
				status_str = "借给"+data.books[i].borrower;
				oper_str="<a href='javascript:return_book("+data.books[i].id+","+i+")'>确认归还</a>";
			}
			if(! (typeof new_ids === 'undefined')){
			    var j;
			    for(j=0;j<new_ids.length;j++){
				    if(new_ids[j] == data.books[i].id){
					    status_str = "<font color='red'>新添加</font>";
					    break;
				    }
			    }
			}
			var item= "<li id='my_book_"+i+"' class='book_item_list_mine'>"
			         +"<img class='book_item_list_mine_img' src='image/book.png'>"
			         +"<span class='book_item_list_mine_title'>"+data.books[i].title+"</span>"
			         +"<span class='book_item_list_mine_operation'>"+oper_str+"</span>"
			         +"<span class='book_item_list_mine_status'>"+status_str+"</span>"
			         +"</li>"
			$("#my_books").append(item);
		}
    },
    error: function (data, status, e)
    {
    	alert(e);
    }
});
}

refresh_my_books();

$.ajax({
	type:"post",
	url: "?app=library&page=book_oper",
	data:"method=query_borrowed_books",
	dataType:'JSON',
	success:function(data){
		var i;
		for(i=0;i<data.count;i++){
			var item= "<li  class='book_item_list_mine'>"
			         +"<img class='book_item_list_mine_img' src='image/book.png'>"
			         +"<span class='book_item_list_mine_title'>"+data.books[i].title+"</span>"
			         +"</li>"
			$("#borrowed_books").append(item);
		}
    },
    error: function (data, status, e)
    {
    	alert(e);
    }
});


</script>

<style type="text/css">
.book_item_list_mine
{
   width:80%;
   padding-top: 5px;
   padding-bottom: 5px;
   height:20px;
}

.book_item_list_mine_img
{
   float:left;
}

.book_item_list_mine_title
{
   padding-left:10px;
   float:left;
}

.book_item_list_mine_status
{
    float:right;
}

.book_item_list_mine_operation
{
    float:right;
    text-align:right;
    padding-right:10px;
    width:20%;
}
</style>

</head>
<body>
<?php the_control_panel();?>
<link href="<?php the_app_location()?>/css/jqModal.css" media="all" type="text/css" rel="stylesheet">
<script src="js/jqModal.js"></script>
<div id='add_book_diag' class='jqmWindow round-top round-bottom'>
<div class='abTitle'><span >添加图书</span><span class='abClose'><input type="image" src='./image/cancel.png' class='jqmClose'></span></div>
<div class='add_book_content'>
<p>Please wait... <img src="image/loading.gif" alt="loading" /></p>
</div>
</div>
<script type="text/javascript">
function register_add_book_diag(){
	$(function(){
		$("#add_book_diag").jqm({
			modal: true, 
		    trigger: 'a.add_book_button',
		    ajax:'@href',
		    target:$(".add_book_content"),
		    onHide: function(h) {
		       h.o.remove(); // remove overlay
		       h.w.fadeOut(222); // hide window
		       refresh_my_books();
		}});
	}); 
}

register_add_book_diag();

</script>
<div id="wraper">
<?php require_once 'library_cpanel.php';?>
<div id="primary">

<div id="loop">
   <ul class="loop-tb">
   <li class="title">
   <span style='float:left'>我的图书：</span>
   <span style='float:right'><a class='add_book_button' href="?app=library&page=add_book">添加图书</a></span>
   <span class="top_right_nevi">

   </span>
   <div style="clear:both"></div>
   </li>
   </ul>
   <ul class="loop-tb" id="my_books"/>
   
</div>

<div id="loop">
   <ul class="loop-tb" id='borrowed_books'>
   <li class="title">
   <span style='float:left'>我的借阅：</span>
   <span class="top_right_nevi">
   
   </span>
   <div style="clear:both"></div>
   </li>

   </ul>
</div>

</div>
<div style="clear:both;"></div>
<?php the_footer();?>
</div>
</body>
</html>