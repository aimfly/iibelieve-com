<?php
$message_func=array();

function add_message_func($type,$func){
	global $message_func;
	$message_func[$type]=$func;
}


function generate_message($type,$args){
	global $message_func;
	if(isset($message_func[$type])){
		call_user_func($message_func[$type],$args);
	}
	return;
}
?>