<?php
include_once 'conf/dbconf.php';
$dbconn = init_db();
function init_db(){
 global $dbserver,$dbuser,$dbpassword,$dbname;;
 $dbconn=mysqli_connect($dbserver,$dbuser,$dbpassword,$dbname);
 if (!$dbconn) {
  echo( "Unable to connect to the database server at this time.");
  exit();
 }

 mysqli_query($dbconn,"set names 'utf8'");
 mysqli_query($dbconn,"SET time_zone = '+8:00'") or die('set timezone failed...');
 return $dbconn;
}

function query_post($count=10,$offset=0){
	$sql="select * from post order by createtime desc limit ".$offset.",".$count;
	return execute_sql($sql);
}

function create_post($args){
	if(!isset($args["content"])){
		return false;
	}
	if(isset($args["type"])){
		$type=$args["type"];
	}
	else{
		$type=9;//default is system post
	}
	if(isset($args["userid"])){
		$userid=$args["userid"];
	}
	else{
		$userid=1;//default is system user
	}
	
	$sql="INSERT INTO `post`(`type`,`createtime`,`userid`,`content`) values(".$type.",now(),".$userid.",'".$args["content"]."')";
	return execute_sql($sql);
}

function set_post_addi($postid,$key,$value){
	$sql="select * from post_addi where `postid`=".$postid." AND `key`='".$key."'";
	$res=execute_sql($sql);
	if($res){
		if(mysqli_num_rows($res)==0){
			//add new post additional 
			$sql="INSERT INTO `post_addi`(`postid`,`key`,`value`)values(".$postid.",'".$key."','".$value."')";
			return execute_sql($sql);
		}
		else{
			$sql="UPDATE `post_addi` set `value`='".$value."' where postid=".$postid." AND `key`='".$key."'";
			return execute_sql($sql);
		}
	}
}

function get_last_id()
{
	global $dbconn;
	return mysqli_insert_id($dbconn);
}

function get_post_addi($postid,$key){
	$sql="select * from post_addi where `postid`=".$postid." AND `key`='".$key."'";
	$res=execute_sql($sql);
	if($res){
		if(mysqli_num_rows($res)==0){
			return false;
		}
		else{
			$row=mysqli_fetch_array($res);
			return $row["value"];
		}
	}
}

function send_message($from,$to,$content){
	$sql="INSERT INTO `message`(`to`, `from`, `content`,`createtime`,`read`) values(".$to.",".$from.",'".$content."',now(),0)";
	return execute_sql($sql);
}

function mark_message_as_read($msg_id){
	$sql="update `message` set `read`=1 where `id`=".$msg_id;
	return execute_sql_silent($sql);
}

function get_message($userid){
    $sql="select * from `message` where `to`=".$userid." or `from`=".$userid." order by createtime desc";
    return execute_sql($sql);
}

function get_unread_message_count($userid){
    $sql="select count(*) from `message` where `read`=0 AND `to`=".$userid;
    $res=execute_sql($sql);
    if($res){
    	$row=mysqli_fetch_array($res);
		return $row[0];
    }else{
    	return 0;
    }
}


function combine_condition(){
	$num=func_num_args();
	$conds=func_get_args();
	$cond_string="";
	for($i=0;$i<$num;$i++){
		if($conds[$i]!=""){
			if($cond_string!=""){
				$cond_string=$cond_string." AND ";
			}
			$cond_string=$cond_string.$conds[$i];
		}
	}
	if($cond_string!=""){
		$cond_string="where ".$cond_string;
	}
	return $cond_string;
}

function execute_sql($sql){
    $result=execute_sql_silent($sql);
	if (!$result[0]) {
		echo $result[1];
		return false;
	}else{
		return $result[1];
	}
}

function db_error(){
	global $dbconn;
	return mysqli_error($dbconn);
}

function execute_sql_silent($sql){
    global $dbconn;
    $result=mysqli_query($dbconn,$sql);
	if (!$result) {
		$ret=array(false,mysqli_error($dbconn)." ".$sql);
		return $ret;
	}else{
		$ret=array(true,$result);
		return $ret;
	}
}

function mail_is_used($email){
 global $dbconn;
 $sql=mysqli_query($dbconn,"select * from users where email='".$email."'");
 if (!$sql) {
  echo mysqli_error();
  return true;
 }
 elseif(mysqli_num_rows($sql)!=0){
  return true;
 }
 else{
  return false;
 }
}

function get_user_name_by_id($userid){
 global $dbconn;
 $result=mysqli_query($dbconn,"select * from users where id=".$userid);
 if(!$result){
  return "";
 }
 else{
  $user=mysqli_fetch_array($result);
  return $user["name"];
 }
}

function user_validate($email,$password){
 global $dbconn;
 $sql=mysqli_query($dbconn,"select * from users where email='".$email."'");
 if (!$sql) {
  echo mysqli_error($dbconn);
  return false;
 }
 elseif(mysqli_num_rows($sql)!=1){
  return false;
 }
 else{
 	$row=mysqli_fetch_array($sql);
 	if($row["password"]!=crypt($password,$password))
 		return false;
 	else
        return $row;
 }
}

function get_records_count($records){
	return mysqli_num_rows($records);
}

function get_next_record($records){
	return mysqli_fetch_array($records);
}


?>