-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 09, 2015 at 12:03 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `iibelieve`
--

-- --------------------------------------------------------

--
-- Table structure for table `application`
--

CREATE TABLE IF NOT EXISTS `application` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `showname` varchar(32) NOT NULL,
  `location` varchar(128) NOT NULL,
  `frontpage` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `application`
--

INSERT INTO `application` (`id`, `name`, `showname`, `location`, `frontpage`) VALUES
(1, 'exchange', '物品交换', 'app/exchange', 'loop'),
(2, 'library', '图书馆', 'app/library', 'loop');

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE IF NOT EXISTS `books` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `title` varchar(256) CHARACTER SET utf8 NOT NULL,
  `author` varchar(256) CHARACTER SET utf8 NOT NULL,
  `image` varchar(256) CHARACTER SET utf8 DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `borrowBy` int(11) NOT NULL DEFAULT '-1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`id`, `user_id`, `title`, `author`, `image`, `status`, `borrowBy`) VALUES
(2, -1, '三毛作品集', '三毛', 'http://img3.douban.com/mpic/s1666750.jpg', 0, 0),
(3, -1, '三毛经典作品（上下）', '三毛', 'http://img4.douban.com/mpic/s2628688.jpg', 0, 0),
(4, -1, '三毛不在撒哈拉', '嘉倩', 'http://img4.douban.com/mpic/s24223089.jpg', 0, 0),
(5, -1, '亲爱的三毛', '（台湾）三毛', 'http://img3.douban.com/mpic/s1433701.jpg', 0, 0),
(6, -1, '三毛全集', '三毛', 'http://img3.douban.com/mpic/s2747794.jpg', 0, 0),
(7, -1, '天才是训练出来的', '薛涌', 'http://img3.douban.com/mpic/s4644284.jpg', 0, -1),
(8, -1, '师从天才', '[美] 罗伯特·卡尼格尔', 'http://img4.douban.com/mpic/s24933157.jpg', 0, -1),
(9, -1, '百年孤独', '[哥伦比亚] 加西亚·马尔克斯', 'http://img3.douban.com/mpic/s6384944.jpg', 0, -1),
(10, -1, '非常营销：娃哈哈--中国成功的实战教案', '吴晓波,胡宏伟', 'http://img3.douban.com/mpic/s11213021.jpg', 0, -1),
(11, -1, '宗庆后与娃哈哈', '罗建幸', 'http://img4.douban.com/mpic/s5903629.jpg', 0, -1),
(12, -1, '呵呵大将: 我が友、三島由紀夫', '竹邑類', 'http://img4.douban.com/mpic/s27222117.jpg', 0, -1),
(13, -1, '吃吃喝喝那点儿事', '', 'http://img3.douban.com/mpic/s6025114.jpg', 0, -1),
(14, -1, '是的，我们能', '加伦·托马斯', 'http://img3.douban.com/mpic/s5961103.jpg', 0, -1),
(15, -1, '出发.才能到达-3000美金周游世界', '朱兆瑞', 'http://img4.douban.com/mpic/s9136149.jpg', 0, -1),
(16, -1, '98阿斯顿飞空间啊方式349ks', '', 'http://img3.douban.com/f/shire/5522dd1f5b742d1e1394a17f44d590646b63871d/pics/book-default-medium.gif', 0, -1),
(17, -1, '雨季不再来', '三毛', 'http://img3.douban.com/mpic/s6565841.jpg', 0, -1),
(18, 7, '天使爱美丽', '罗成群', 'http://img3.douban.com/mpic/s5666483.jpg', 0, -1),
(19, 7, '三毛作品集', '三毛', 'http://img3.douban.com/mpic/s1666750.jpg', 0, -1),
(20, 7, '三毛从军记全集', '张乐平', 'http://img3.douban.com/mpic/s2579020.jpg', 0, -1),
(21, 7, '呵呵后', '', 'http://img3.douban.com/mpic/s26712361.jpg', 0, -1),
(22, 7, '偉大的咻啦啦砰', '萬城目學', 'http://img3.douban.com/mpic/s8354640.jpg', 0, -1),
(23, 7, '偉大的咻啦啦砰', '萬城目學', 'http://img3.douban.com/mpic/s8354640.jpg', 0, -1),
(24, 7, '偉大的咻啦啦砰', '萬城目學', 'http://img3.douban.com/mpic/s8354640.jpg', 0, -1),
(25, 7, '巴啦啦小魔仙1', '迪文文化传播限公司', 'http://img4.douban.com/mpic/s5955229.jpg', 0, -1),
(26, 7, 'わたしの少女マンガ史―別マから花ゆめ、LaLaへ', '小長井 信昌', 'http://img4.douban.com/mpic/s7020217.jpg', 0, -1),
(27, 7, 'Lala Pipo', 'Hideo Okuda', 'http://img3.douban.com/mpic/s3723625.jpg', 0, -1);

-- --------------------------------------------------------

--
-- Table structure for table `exc_goods`
--

CREATE TABLE IF NOT EXISTS `exc_goods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `createtime` datetime DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(1024) DEFAULT NULL,
  `photo_loc` varchar(1024) DEFAULT NULL,
  `evaluation` int(11) DEFAULT NULL,
  `cangive` tinyint(1) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exc_transaction`
--

CREATE TABLE IF NOT EXISTS `exc_transaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `createtime` datetime NOT NULL,
  `type` int(11) NOT NULL DEFAULT '1',
  `goods_id1` int(11) NOT NULL,
  `goods_id2` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `goods_id1` (`goods_id1`,`goods_id2`,`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE IF NOT EXISTS `message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `to` int(11) NOT NULL,
  `from` int(11) NOT NULL,
  `content` varchar(140) NOT NULL,
  `createtime` datetime NOT NULL,
  `read` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`id`, `to`, `from`, `content`, `createtime`, `read`) VALUES
(1, 4, 6, '阿斯顿', '2015-07-03 16:48:19', 0),
(2, 7, 7, '呵呵', '2015-07-03 16:54:05', 1),
(3, 7, 7, 'haha', '2015-07-03 17:49:18', 1);

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE IF NOT EXISTS `post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `createtime` datetime NOT NULL,
  `type` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `content` varchar(256) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`id`, `createtime`, `type`, `userid`, `content`) VALUES
(1, '2015-07-02 00:00:00', 1, 1, 'qerwerwgrgrgg'),
(2, '2015-07-03 15:39:04', 9, 1, '<span style="margin-left:10%;float:left"><img src="user_portrait/boy_default_portrait_thumb.jpg"></span><span style="margin-left:5%;float:left">xx加入这个大家庭。大家欢迎！</span>'),
(3, '2015-07-03 15:41:18', 9, 1, '<span style="margin-left:10%;float:left"><img src="user_portrait/boy_default_portrait_thumb.jpg"></span><span style="margin-left:5%;float:left">yy加入这个大家庭。大家欢迎！</span>'),
(4, '2015-07-03 16:03:23', 9, 1, '<span style="margin-left:10%;float:left"><img src="user_portrait/boy_default_portrait_thumb.jpg"></span><span style="margin-left:5%;float:left">dd加入这个大家庭。大家欢迎！</span>'),
(5, '2015-07-03 16:17:23', 9, 1, '<span style="margin-left:10%;float:left"><img src="user_portrait/boy_default_portrait_thumb.jpg"></span><span style="margin-left:5%;float:left">sdfsdf加入这个大家庭。大家欢迎！</span>'),
(6, '2015-07-03 16:53:51', 9, 1, '<span style="margin-left:10%;float:left"><img src="user_portrait/boy_default_portrait_thumb.jpg"></span><span style="margin-left:5%;float:left">星空加入这个大家庭。大家欢迎！</span>');

-- --------------------------------------------------------

--
-- Table structure for table `post_addi`
--

CREATE TABLE IF NOT EXISTS `post_addi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `postid` int(11) NOT NULL,
  `key` varchar(32) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `postid` (`postid`,`key`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `post_addi`
--

INSERT INTO `post_addi` (`id`, `postid`, `key`, `value`) VALUES
(1, 0, 'flower', ''),
(2, 3, 'flower', '6,7'),
(3, 2, 'flower', '7'),
(4, 4, 'flower', '6,7'),
(5, 5, 'flower', '6,7,127.0.0.1'),
(6, 6, 'flower', '127.0.0.1');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(16) NOT NULL,
  `password` varchar(16) NOT NULL,
  `email` varchar(128) NOT NULL,
  `sex` varchar(16) NOT NULL,
  `portrait` varchar(128) DEFAULT NULL,
  `privilege` int(11) NOT NULL DEFAULT '1',
  `locked` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `password`, `email`, `sex`, `portrait`, `privilege`, `locked`) VALUES
(1, 'system', 'asdf123456', 'system@iibelieve.com', '', '', 1, 1),
(2, 'admin', 'asdf123', 'admin@iibelieve.com', '', '', 1, 0),
(3, 'xx', '$1$tp2.ye..$qAo0', 'a@a.com', 'male', '', 1, 0),
(4, 'yy', '$1$sV4.Fh1.$J8MX', 'b@a.com', 'male', '', 1, 0),
(5, 'dd', '$1$X6..UA2.$A.Wo', 'a@ab.com', 'male', '', 1, 0),
(6, 'sdfsdf', '12eMC4Wi9/C9o', 'd@aa.com', 'male', '', 1, 0),
(7, '星空', '12tir.zIbWQ3c', 'dd@d.com', 'male', '', 1, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
