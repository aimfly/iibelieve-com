function register_sendmsg(){
	$(function(){
		$('.send_msg').click(function(){
			var contant=$('.msgcontant').val();
			var to=$('.to').val();
			var from=$('.from').val();
			if(contant==""){
				alert("消息内容为空");
				return;
			}
			var dataString="to="+to+"&from="+from+"&contant="+contant;
			$.ajax({
				type : "POST",
				url : "?page=sendmsg",
				data : dataString,
				success: function(data){
				    if(data=="success"){
				    	$("#sendtip").addClass("green_text");
				    	$("#sendtip").html("发送成功");
				    	$("#sendmsg_diag").jqmHide();
				    	$('.msgcontant').val("");
				    }
				    else{
				    	alert(data);
				    	$("#sendtip").addClass("striking_text");
				    	$("#sendtip").html("发送失败");
				    }
			    }
			});
			
		});
	});
}