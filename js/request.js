function register_sendmsg_diag(){
	$(function(){
		$("#sendmsg_diag").jqm({modal: true, 
			                   trigger: 'a.sendmsg',
			                   ajax:'@href',
			                   target:$(".sendmsg_contant"),
			                   onHide: function(h) { 
		                          h.o.remove(); // remove overlay
		                          h.w.fadeOut(222); // hide window
		                       }});
	}); 
}

function register_message_page(){
	$(function(){
		$('.no_read_message').hover(function(){
			var ob=$(this);
			var msg_id=$(this).children(".msg_id").val();
			var dataString="markid="+msg_id;
			$.ajax({
				type:"POST",
				url:"?page=message",
				data:dataString,
				dataType: 'json',
				success:function(data,status){
				    
				    if(data.result=="success"){
				    	ob.removeClass("no_read_message");
				    	ob.addClass("read_message");
				    	ob.children(".message_icon").removeClass("no_read_message_icon");
				    	ob.children(".message_icon").addClass("read_message_icon");
				    	$('.msg_tip').html("消息("+data.addtext+")");
				    }else{
				    	alert(data);
				    }
			    },
			    error: function (data, status, e)
	            {
	                alert(e);
	            }
			});
		});
	});
	
}


function upbuttom_mousemove(e) {
    var offL, offR, inpStart
    offL = $(this).offset().left;
    offT = $(this).offset().top;
    aaa= $(this).find("input").width();
    $(this).find("input").css({
        left:e.pageX-offL-100,
        top:e.pageY-offT-10
    });
}

function register_logon_page()
{
	$(function() {
		email_is_ok=false;
		pass_is_ok=false;
		$("#portrait").mousemove(upbuttom_mousemove);
		$('#regEmail').blur(function(){
			var email=$('#regEmail').val();
			if(email==""){
				$('.mail_tip').removeClass("striking_text");
				$('.mail_tip').html("在此处输入您的注册邮箱");
			}
			else{
				var pattern = /^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(\.[a-zA-Z0-9_-])+/;
				if(pattern.test(email)){
					var dataString="reqType=checkmail&email="+email;
					$.ajax({
						type : "POST",
						url : "?page=logon",
						data : dataString,
						success : function(data) {
							if(data=="failed"){
								$('.mail_tip').addClass("striking_text");
								$('.mail_tip').html("<img src='image/cancel.png'>邮箱已被使用");
								email_is_ok=false;
							}
							else if(data=="sucess"){
								$('.mail_tip').html("<img src='image/accept.png'>");
								email_is_ok=true;
							}
						}
					});
				}
				else{
					$('.mail_tip').addClass("striking_text");
					$('.mail_tip').html("<img src='image/cancel.png'>邮箱格式错误");
					email_is_ok=false;
				}
			}
		});
		
		$('#passwd').blur(function(){
			var pass1=$('#passwd').val();
			var pass2=$('#re-passwd').val();
			if(pass1=="" && pass2==""){
				$('.repw_tip').removeClass("striking_text");
				$('.pw_tip').html("请在此处输入您的注册密码");
				$('.repw_tip').html("请再次输入您的注册密码");
				pass_is_ok=false;
			}
			else if(pass1==pass2){
				$('.pw_tip').html("");
				$('.repw_tip').html("<img src='image/accept.png'>");
			    pass_is_ok=true;
			}else{
				$('.repw_tip').addClass("striking_text");
				$('.pw_tip').html("");
				$('.repw_tip').html("<img src='image/cancel.png'>两次输入的密码不匹配");
				pass_is_ok=false;
			}
		});
		
		$('#re-passwd').blur(function(){
			var pass1=$('#passwd').val();
			var pass2=$('#re-passwd').val();
			if(pass1=="" && pass2==""){
				$('.repw_tip').removeClass("striking_text");
				$('.pw_tip').html("请在此处输入您的注册密码");
				$('.repw_tip').html("请再次输入您的注册密码");
				pass_is_ok=false;
			}
			else if(pass1==pass2){
				$('.pw_tip').html("");
				$('.repw_tip').html("<img src='image/accept.png'>");
			    pass_is_ok=true;
			}else{
				$('.repw_tip').addClass("striking_text");
				$('.pw_tip').html("");
				$('.repw_tip').html("<img src='image/cancel.png'>两次输入的密码不匹配");
				pass_is_ok=false;
			}
		});
		$('#register').click(function(){
			var email=$('#regEmail').val();
			var password=$('#passwd').val();
			var username=$('#username').val();
			var sex=$('input[name="sex"]:checked').val();
			var invitecode=$('#invitecode').val();
			var portraitid=$('#portraitid').val();
			if(sex=='unknown'){
				alert("No...it's kidding, you should chose one between 弟兄 and 姐妹");
				exit;
			}
			if(!pass_is_ok || !email_is_ok || username==""){
				alert("无效的注册信息！可能的原因：注册邮箱无效或已被使用，密码无效，昵称为空");
				return;
			}
			var dataString="reqType=register&email="+email+"&password="+password+"&username="+username+"&portraitid="+portraitid+"&sex="+sex+"&invitecode="+invitecode;
			$.ajax({
				type : "POST",
				url : "?page=logon",
				data : dataString,
				success : function(data) {
					if(data=="success"){
						window.location.href="?page=home";
					}else{
						alert("注册失败，请按照提示输入注册信息，如果不能注册，请联系管理员");
						$('#registertip').addClass('must_fill');
						$('#registertip').html('注册失败，请稍后重试');
					}
				}
			});
		});
		
		$("#fileToUpload").change(function(){
        	uploadPortrait();
        });
	});
}

function uploadPortrait(){
	$("#portrait").mousemove(function(e){});
	var ori_image=$("#portrait_image").attr("src");
	$("#portrait_image").attr("src","./image/loading.gif");
	
	
	if($('#portraitid').val()!=''){
		var dataString="removePortrait="+$('#portraitid').val();
		$.ajax({
			 type:"POST",
			 url:"uploadportrait.php",
			 data:dataString,
			 sucess:function(data){
			 	;
		 	 }
		});
	}
	
	$.ajaxFileUpload(
            { url:'uploadportrait.php',
               secureuri:false,
               fileElementId:'fileToUpload',
               dataType: 'json',
               success: function (data, status)
               {
                   if(typeof(data.error) != 'undefined')
                   {
                       if(data.error != '')
                       {
                           alert(data.error);
                           $("#portrait").mousemove(upbuttom_mousemove);
                           $("#portrait_image").attr("src",ori_image);
                       }else
                       {
                    	   $("#portrait").mousemove(upbuttom_mousemove);
                           $('#portrait_image').attr("src",data.msg);
                           $('#portraitid').val(data.photoid);
                       }
                   }
                   $("#fileToUpload").change(function(){
                	   uploadPortrait();
                   });
                   return false;
               },
               error: function (data, status, e)
               {
            	   $("#portrait").mousemove(upbuttom_mousemove);
            	   $("#portrait_image").attr("src",ori_image);
                   $("#fileToUpload").change(function(){
                	   uploadPortrait();
                   });
               }
            });
}

