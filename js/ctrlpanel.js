function register_ctrlpannel(){
	$(function(){
		$("#login_button").click(function(){
			
			if($("#quick_login").hasClass("no_display")){
				$("#quick_login").removeClass("no_display");
			}else{
				$("#quick_login").addClass("no_display");
			}
		});
		$('#logon').click(function(){
			var email=$('#email').val();
			var password=$('#password').val();
			if(email==""||password==""){
				$('#logontip').addClass('must_fill');
				$('#logontip').html('无效的邮箱或密码');
			}
			var dataString="reqType=logon&email="+email+"&password="+password;
			$.ajax({
				type : "POST",
				url : "?page=logon",
				data : dataString,
				success : function(data) {
					if(data=="failed"){
						$('#logontip').addClass('must_fill');
						$('#logontip').html('无效的邮箱或密码');
					}
					else if(data=="locked"){
						$('#logontip').addClass('must_fill');
						$('#logontip').html('账户锁定，请联系管理员');
					}
					else if(data=="sucess"){
						location.reload();
					}
				}
			});
		});
	});
}